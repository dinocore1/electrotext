# ElectroText

A way to create electronic schematics by writing code

### Example

```

symbol ATtiny45 {
    VCC, PB0, PB1, PB2, PB3, PB4, PB5, GND
}


N 3V3;

U1 Attiny45;
U1.VCC -> 3V3;
U1.PB0 -> STATUS_LED;
U1.GND -> GND;


// bypass cap
C? 0.1F;
. -> 3V3;
. -> GND;

// status LED
D?;
. -> STATUS_LED;
. -> R? -> GND;


// Power LED
D?;
. -> 3V3;
. -> R? -> GND;

C
R
L





```